package com.zczapran.orl.ontology;

import java.util.Collection;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public interface Ontology {

	public Collection<String> getRelated(String concept);
}
