package com.zczapran.orl.ontology;

import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.map.hash.TObjectIntHashMap;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Set;

/**
 * A simple class for storing relations between concepts.
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class SimpleOntology implements Ontology {

	final private TObjectIntHashMap<String> concepts;
	final private ArrayList<String> conceptIndexes;
	final private TIntObjectHashMap<TIntSet> relations;

	public SimpleOntology() {
		this.relations = new TIntObjectHashMap<>();
		this.concepts = new TObjectIntHashMap<>();
		this.conceptIndexes = new ArrayList<>();
	}

	/**
	 * Creates unidirectional relationship from concept1 to concept2.
	 *
	 * @param concept1
	 * @param concept2
	 */
	public void addRelation(String concept1, String concept2) {
		int index1;
		int index2;

		if (!this.concepts.containsKey(concept1)) {
			index1 = this.concepts.size();
			this.concepts.put(concept1, index1);
			this.conceptIndexes.add(concept1);
		} else {
			index1 = this.concepts.get(concept1);
		}

		if (!this.concepts.containsKey(concept2)) {
			index2 = this.concepts.size();
			this.concepts.put(concept2, index2);
			this.conceptIndexes.add(concept2);
		} else {
			index2 = this.concepts.get(concept2);
		}

		if (!this.relations.containsKey(index1)) {
			this.relations.put(index1, new TIntHashSet());
		}

		TIntSet related = this.relations.get(index1);
		related.add(index2);
	}

	/**
	 * Gets list of all registered concepts.
	 *
	 * @return Set of concepts
	 */
	public Set<String> getConcepts() {
		return this.concepts.keySet();
	}

	/**
	 * Prepares and returns list of concepts given concept is related to.
	 *
	 * @param concept
	 *
	 * @return Array of related concepts
	 */
	@Override
	public Collection<String> getRelated(String concept) {
		Collection<String> related = new LinkedList<>();

		if (this.concepts.containsKey(concept)) {
			int index = this.concepts.get(concept);

			if (this.relations.containsKey(index)) {
				int[] indexes = this.relations.get(index).toArray();
				for (int i = 0; i < indexes.length; i++) {
					related.add(this.conceptIndexes.get(indexes[i]));
				}
			}
		}

		return related;
	}
}
