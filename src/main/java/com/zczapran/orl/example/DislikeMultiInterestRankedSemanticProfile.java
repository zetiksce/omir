package com.zczapran.orl.example;

import com.zczapran.orl.document.ConceptDocument;
import com.zczapran.orl.ontology.Ontology;
import com.zczapran.orl.profile.DislikeProfile;
import com.zczapran.orl.profile.ProfileFactory;
import com.zczapran.orl.profile.SimpleDislikeProfile;
import com.zczapran.orl.profile.multiinterest.MultiInterestProfile;
import com.zczapran.orl.profile.rankedsemantic.RankedSemanticProfile;
import com.zczapran.orl.profile.rankedsemantic.RankedSemanticProfileFactory;
import com.zczapran.orl.strategy.adddocument.AddDocumentStrategy;
import com.zczapran.orl.strategy.adddocument.StrategyOne;
import com.zczapran.orl.strategy.adddocument.helper.LogarithmicScoreModifier;
import com.zczapran.orl.strategy.adddocument.helper.ParallelScorer;
import com.zczapran.orl.strategy.adddocument.helper.ScoreModifier;
import com.zczapran.orl.strategy.adddocument.helper.Scorer;
import com.zczapran.orl.strategy.scoredocument.BestScoreStrategy;
import com.zczapran.orl.strategy.scoredocument.ScoreDocumentStrategy;

/**
 * Example profile.
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class DislikeMultiInterestRankedSemanticProfile implements DislikeProfile<ConceptDocument> {

	private DislikeProfile<ConceptDocument> profile;

	public DislikeMultiInterestRankedSemanticProfile(Ontology ontology) {
		ScoreModifier<RankedSemanticProfile> scoreModifier = new LogarithmicScoreModifier<>();
		ProfileFactory<RankedSemanticProfile> profileFactory = new RankedSemanticProfileFactory(ontology);
		AddDocumentStrategy<ConceptDocument, RankedSemanticProfile> addStrategy = new StrategyOne<>(profileFactory, 10, scoreModifier);
		Scorer<ConceptDocument, RankedSemanticProfile> scorer = new ParallelScorer<>();
		ScoreDocumentStrategy<ConceptDocument, RankedSemanticProfile> scoreStrategy = new BestScoreStrategy<>(scorer);
		profile = new SimpleDislikeProfile<>(
				new MultiInterestProfile<>(addStrategy, scoreStrategy),
				new MultiInterestProfile<>(addStrategy, scoreStrategy));
	}

	public DislikeMultiInterestRankedSemanticProfile(
			AddDocumentStrategy<ConceptDocument, RankedSemanticProfile> likeAddStrategy,
			AddDocumentStrategy<ConceptDocument, RankedSemanticProfile> dislikeAddStrategy,
			ScoreDocumentStrategy<ConceptDocument, RankedSemanticProfile> scoreStrategy) {
		profile = new SimpleDislikeProfile<>(
				new MultiInterestProfile<>(likeAddStrategy, scoreStrategy),
				new MultiInterestProfile<>(dislikeAddStrategy, scoreStrategy));
	}

	public DislikeMultiInterestRankedSemanticProfile(
			AddDocumentStrategy<ConceptDocument, RankedSemanticProfile> likeAddStrategy,
			AddDocumentStrategy<ConceptDocument, RankedSemanticProfile> dislikeAddStrategy) {
		this(likeAddStrategy, dislikeAddStrategy, new BestScoreStrategy<>(new ParallelScorer<ConceptDocument, RankedSemanticProfile>()));
	}

	public DislikeMultiInterestRankedSemanticProfile(AddDocumentStrategy<ConceptDocument, RankedSemanticProfile> addStrategy) {
		this(addStrategy, addStrategy);
	}

	public DislikeMultiInterestRankedSemanticProfile(
			AddDocumentStrategy<ConceptDocument, RankedSemanticProfile> addStrategy,
			ScoreDocumentStrategy<ConceptDocument, RankedSemanticProfile> scoreStrategy) {
		this(addStrategy, addStrategy, scoreStrategy);
	}

	@Override
	public void dislike(ConceptDocument document) {
		profile.dislike(document);
	}

	@Override
	public void add(ConceptDocument document) {
		profile.add(document);
	}

	@Override
	public double score(ConceptDocument document) {
		return profile.score(document);
	}

	@Override
	public int size() {
		return profile.size();
	}

}
