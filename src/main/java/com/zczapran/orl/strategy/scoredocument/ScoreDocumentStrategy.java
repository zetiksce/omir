package com.zczapran.orl.strategy.scoredocument;

import com.zczapran.orl.document.Document;
import com.zczapran.orl.profile.Profile;
import com.zczapran.orl.strategy.Strategy;
import java.util.List;

/**
 * Interface for clases that can calculate score for a document in given list of
 * profiles.
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public interface ScoreDocumentStrategy<T extends Document, V extends Profile<T>> extends Strategy<T, V> {

	/**
	 * Calculates score for a document and given list of profiles.
	 *
	 * @param profiles
	 * @param document
	 *
	 * @return Normalised score [0..1]
	 */
	public double score(List<V> profiles, T document);

}
