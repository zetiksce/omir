package com.zczapran.orl.strategy.scoredocument;

import com.zczapran.orl.document.Document;
import com.zczapran.orl.profile.Profile;
import com.zczapran.orl.strategy.adddocument.helper.Scorer;
import com.zczapran.orl.util.ArrayOperations;
import com.zczapran.orl.util.Pair;
import java.util.List;

/**
 * Chooses the best score from single document/profile scores.
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class BestScoreStrategy<T extends Document, V extends Profile<T>> implements ScoreDocumentStrategy<T, V> {

	private final Scorer<T, V> scorer;

	/**
	 * Constructor.
	 *
	 * @param scorer
	 */
	public BestScoreStrategy(Scorer<T, V> scorer) {
		this.scorer = scorer;
	}

	@Override
	public double score(List<V> profiles, T document) {
		double[] scores = scorer.score(profiles, document);

		Pair<Integer, Double> pair = ArrayOperations.max(scores);

		return pair.right();
	}

}
