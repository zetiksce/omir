package com.zczapran.orl.strategy.scoredocument;

import com.zczapran.orl.document.Document;
import com.zczapran.orl.profile.Profile;
import java.util.List;

/**
 * Scores is an average weight of document/profile scores.
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class AverageWeightScoreStrategy<T extends Document, V extends Profile<T>> implements ScoreDocumentStrategy<T, V> {

	@Override
	public double score(List<V> profiles, T document) {
		double sum = 0.0;
		int numberOfDocuments = 0;

		for (V profile : profiles) {
			sum += profile.score(document) * profile.size();
			numberOfDocuments += profile.size();
		}

		return sum / numberOfDocuments;
	}

}
