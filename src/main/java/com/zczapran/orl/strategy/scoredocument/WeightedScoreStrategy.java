package com.zczapran.orl.strategy.scoredocument;

import com.zczapran.orl.document.Document;
import com.zczapran.orl.profile.weighted.Weighted;
import java.util.List;

/**
 * Score is a weighted sum of all document/profile scores.
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class WeightedScoreStrategy<T extends Document, V extends Weighted<T>> implements ScoreDocumentStrategy<T, V> {

	@Override
	public double score(List<V> profiles, T document) {
		double sum = 0.0;
		int sumOfWeights = 0;

		for (V profile : profiles) {
			sum += profile.score(document) * profile.weight();
			sumOfWeights += profile.weight();
		}

		return sum / sumOfWeights;
	}

}