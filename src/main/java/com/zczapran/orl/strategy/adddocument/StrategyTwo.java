package com.zczapran.orl.strategy.adddocument;

import com.zczapran.orl.document.Document;
import com.zczapran.orl.profile.Mergable;
import com.zczapran.orl.profile.ProfileFactory;
import com.zczapran.orl.profile.Scorable;
import com.zczapran.orl.strategy.adddocument.helper.ScoreModifier;
import com.zczapran.orl.strategy.scoredocument.ScoreDocumentStrategy;
import com.zczapran.orl.util.Pair;
import java.util.List;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class StrategyTwo<T extends Document, V extends Mergable<T, V> & Scorable<T, V>> extends MergeAndCreateStrategy<T, V> {

	final ProfileFactory<V> factory;
	final int maximumSize;
	double sum = 0.0;
	int counter = 0;
	private final ScoreDocumentStrategy<T, V> scoreDocument;

	/**
	 * Constructs StrategyTwo.
	 *
	 * @param factory Profile factory.
	 */
	public StrategyTwo(ProfileFactory<V> factory, int maximumSize, ScoreDocumentStrategy<T, V> scoreDocument, ScoreModifier<V> scoreModifier) {
		super(scoreModifier);
		this.factory = factory;
		this.maximumSize = maximumSize;
		this.scoreDocument = scoreDocument;
	}

	@Override
	public void add(List<V> profiles, T document) {
		if (profiles.size() > 0) {
			sum += scoreDocument.score(profiles, document);
			counter++;

			Pair<Integer, Double> closest = findClosest(profiles, document);

			if (closest.right() > Math.sqrt(sum / counter)) {
				profiles.get(closest.left()).add(document);
				return;
			}
		}

		if (profiles.size() == maximumSize) {
			// merge two closest centroids
			Pair<Integer, Integer> closestProfiles = findClosest(profiles);
			profiles.get(closestProfiles.left().intValue()).merge(profiles.get((closestProfiles.right().intValue())));
			profiles.remove(closestProfiles.right().intValue());
		}

		V profile = factory.getProfile();
		profile.add(document);
		profiles.add(profile);
	}

}