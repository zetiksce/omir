package com.zczapran.orl.strategy.adddocument.helper;

import com.zczapran.orl.document.Document;
import com.zczapran.orl.profile.DocumentSetRetrievable;
import com.zczapran.orl.util.ArrayOperations;
import com.zczapran.orl.util.Pair;
import java.util.List;

/**
 * Class-wrapper for disintegrate method.
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class Disintegrator<T extends Document, V extends DocumentSetRetrievable<T>> {

	/**
	 * Disintegrates profile by redistrubiting its documents.
	 *
	 * @param subject
	 * @param profiles
	 *
	 * @return
	 */
	public List<V> disintegrate(V subject, List<V> profiles) {
		Scorer<T, V> scorer = new SequentialScorer();

		// for each document find a more similar profile then the current
		for (T document : subject.documents()) {
			double currentScore = subject.score(document);
			double[] scoring = scorer.score(profiles, document);
			Pair<Integer, Double> max = ArrayOperations.max(scoring);
			if (currentScore < max.right()) {
				profiles.get(max.left()).add(document);
			}
		}

		// @todo: cluster the remaining documents
		profiles.add(subject);

		return profiles;
	}

}
