package com.zczapran.orl.strategy.adddocument.helper;

import com.zczapran.orl.profile.Scorable;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public interface ScoreModifier<V extends Scorable> {

	/**
	 * Modifies score between to profiles.
	 *
	 * @param profile1
	 * @param profile2
	 *
	 * @return
	 */
	public double modify(V profile1, V profile2);

}
