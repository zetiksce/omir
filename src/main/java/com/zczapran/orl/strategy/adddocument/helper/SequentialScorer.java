package com.zczapran.orl.strategy.adddocument.helper;

import com.zczapran.orl.document.Document;
import com.zczapran.orl.profile.Profile;
import java.util.List;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class SequentialScorer<T extends Document, V extends Profile<T>> implements Scorer<T, V> {

	@Override
	public double[] score(List<V> profiles, T document) {
		double[] scores = new double[profiles.size()];

		int i = 0;
		for (V profile : profiles) {
			scores[i++] = profile.score(document);
		}

		return scores;
	}

}
