package com.zczapran.orl.strategy.adddocument.helper;

import com.zczapran.orl.profile.Scorable;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class LogarithmicScoreModifier<V extends Scorable>
		implements ScoreModifier<V> {
	private final double a;
	private final double b;
	private final double c;

	public LogarithmicScoreModifier(double a, double b, double c) {
		this.a = a;
		this.b = b;
		this.c = c;

	}

	public LogarithmicScoreModifier() {
		this(0.5, 1.5, 1.0);
	}

	/**
	 * Modifies the score so the larger the profiles are the lower score between
	 * them.
	 *
	 * @param profile1
	 * @param profile2
	 *
	 * @return
	 */
	@Override
	public double modify(V profile1, V profile2) {
		return ((a + profile1.score(profile2)) / b) / (Math.log(Math.max(profile1.size(), profile2.size())) + c);
	}

}
