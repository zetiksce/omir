package com.zczapran.orl.strategy.adddocument;

import com.zczapran.orl.document.Document;
import com.zczapran.orl.profile.weighted.Weighted;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class ExpireDecoratorStrategy<T extends Document, V extends Weighted<T>> implements AddDocumentStrategy<T, V> {

	private final AddDocumentStrategy<T, V> substrategy;
	private final double decreaseStep;

	/**
	 * Constructs StrategyTwo.
	 *
	 * @param factory Profile factory.
	 */
	public ExpireDecoratorStrategy(AddDocumentStrategy<T, V> substrategy) {
		this(substrategy, 0.02);
	}

	public ExpireDecoratorStrategy(AddDocumentStrategy<T, V> substrategy, double decreaseStep) {
		this.substrategy = substrategy;
		this.decreaseStep = decreaseStep;
	}

	@Override
	public void add(List<V> profiles, T document) {
		substrategy.add(profiles, document);

		List<V> expired = new LinkedList<>();
		for (V profile : profiles) {
			profile.increaseWeight(-1 * decreaseStep);
			if (profile.weight() < 0) {
				expired.add(profile);
			}
		}

		if (expired.size() > 0) {
			System.out.println("Removing " + expired.size() + " profiles.");
		}

		profiles.removeAll(expired);
	}

}