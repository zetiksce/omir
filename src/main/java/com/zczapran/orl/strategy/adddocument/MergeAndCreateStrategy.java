package com.zczapran.orl.strategy.adddocument;

import com.zczapran.orl.document.Document;
import com.zczapran.orl.profile.Mergable;
import com.zczapran.orl.profile.Scorable;
import com.zczapran.orl.strategy.adddocument.helper.ScoreModifier;
import com.zczapran.orl.strategy.adddocument.helper.Scorer;
import com.zczapran.orl.strategy.adddocument.helper.SequentialScorer;
import com.zczapran.orl.util.ArrayOperations;
import com.zczapran.orl.util.Pair;
import java.util.List;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
abstract public class MergeAndCreateStrategy<T extends Document, V extends Mergable<T, V> & Scorable<T, V>> implements AddDocumentStrategy<T, V> {

	final private ScoreModifier<V> scoreModifier;

	public MergeAndCreateStrategy(ScoreModifier<V> scoreModifier) {
		this.scoreModifier = scoreModifier;
	}

	/**
	 * Finds the closest profile to the document.
	 *
	 * @param profiles
	 * @param document
	 *
	 * @return Position of the profile in the profiles list.
	 */
	protected Pair<Integer, Double> findClosest(List<V> profiles, T document) {
		Scorer<T, V> scorer = new SequentialScorer<>();
		double[] scores = scorer.score(profiles, document);

		return ArrayOperations.max(scores);
	}

	/**
	 * Finds two closest profiles.
	 *
	 * @param profiles
	 *
	 * @return Position of the closest profiles.
	 */
	protected Pair<Integer, Integer> findClosest(List<V> profiles) {
		double max = 0.0;
		Pair<Integer, Integer> pair = null;

		for (int i = 0; i < profiles.size(); i++) {
			for (int j = i + 1; j < profiles.size(); j++) {
				double score = scoreModifier.modify(profiles.get(i), profiles.get(j));
				if (pair == null || max < score) {
					pair = new Pair<>(i, j);
					max = score;
				}
			}
		}
		// System.out.println(profiles.get(pair.left()).size() + " : " + profiles.get(pair.right()).size() + " : " + max);
		return pair;
	}

}
