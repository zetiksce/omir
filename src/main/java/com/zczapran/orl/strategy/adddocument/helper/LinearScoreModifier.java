package com.zczapran.orl.strategy.adddocument.helper;

import com.zczapran.orl.profile.Scorable;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class LinearScoreModifier<V extends Scorable>
		implements ScoreModifier<V> {

	/**
	 * Doesn't modify the score between two profiles.
	 *
	 * @param profile1
	 * @param profile2
	 *
	 * @return
	 */
	@Override
	public double modify(V profile1, V profile2) {
		return profile1.score(profile2);
	}

}
