package com.zczapran.orl.strategy.adddocument;

import com.zczapran.orl.document.Document;
import com.zczapran.orl.profile.Profile;
import com.zczapran.orl.strategy.Strategy;
import java.util.List;

/**
 * Interface for strategires that are capable of adding a document to list of profiles.
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public interface AddDocumentStrategy<T extends Document, V extends Profile<T>> extends Strategy<T, V> {

	/**
	 * Performs operation of adding a document to given list of profiles.
	 *
	 * @param profiles
	 * @param document
	 */
	public void add(List<V> profiles, T document);

}
