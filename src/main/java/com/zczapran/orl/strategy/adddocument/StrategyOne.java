package com.zczapran.orl.strategy.adddocument;

import com.zczapran.orl.document.Document;
import com.zczapran.orl.profile.Mergable;
import com.zczapran.orl.profile.ProfileFactory;
import com.zczapran.orl.profile.Scorable;
import com.zczapran.orl.strategy.adddocument.helper.ScoreModifier;
import com.zczapran.orl.util.Pair;
import java.util.List;

/**
 * Merge-and-create strategy as described in paper ...
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class StrategyOne<T extends Document, V extends Mergable<T, V> & Scorable<T, V>> extends MergeAndCreateStrategy<T, V> {

	ProfileFactory<V> factory;
	int maximumSize;

	/**
	 * Constructs MergeAndCreate strategy.
	 *
	 * @param factory Profile factory.
	 */
	public StrategyOne(ProfileFactory<V> factory, int maximumSize, ScoreModifier<V> scoreModifier) {
		super(scoreModifier);
		this.factory = factory;
		this.maximumSize = maximumSize;
	}

	/**
	 * Adds the document to the list of profiles. Using algorithm described in
	 * ....
	 *
	 * @param profiles List of profiles
	 * @param document Document to be added
	 */
	@Override
	public void add(List<V> profiles, T document) {
		if (profiles.size() == maximumSize) {
			// move closest centroid toward point
			profiles.get(findClosest(profiles, document).left()).add(document);

			// merge two closest centroids
			Pair<Integer, Integer> closest = findClosest(profiles);
			profiles.get(closest.left().intValue()).merge(profiles.get((closest.right().intValue())));
			profiles.remove(closest.right().intValue());
		}

		// create new centroid equal to the data point
		V newProfile = factory.getProfile();
		newProfile.add(document);
		profiles.add(newProfile);
	}

}
