package com.zczapran.orl.strategy.adddocument.helper;

import com.zczapran.orl.document.Document;
import com.zczapran.orl.profile.Profile;
import java.util.List;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public interface Scorer<T extends Document, V extends Profile<T>> {

	public double[] score(List<V> profiles, T document);

}
