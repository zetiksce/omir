package com.zczapran.orl.strategy.adddocument.helper;

import com.zczapran.orl.document.Document;
import com.zczapran.orl.profile.Profile;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class ParallelScorer<T extends Document, V extends Profile<T>> implements Scorer<T, V> {

	@Override
	public double[] score(List<V> profiles, T document) {
		ForkJoinPool pool = new ForkJoinPool();

		double[] results = new double[profiles.size()];

		Profile<T>[] ts = profiles.toArray(new Profile[profiles.size()]);
		Scoring action = new Scoring(ts, document, 0, ts.length, results);
		pool.invoke(action);
		pool.shutdown();

		return results;
	}

	private class Scoring extends RecursiveAction {

		private final Profile<T>[] profiles;
		private final T document;
		private final int start;
		private final int length;
		private final double[] results;

		public Scoring(Profile<T>[] profiles, T document, int start, int length, double[] results) {
			this.profiles = profiles;
			this.document = document;
			this.start = start;
			this.length = length;
			this.results = results;
		}

		@Override
		protected void compute() {
			if (length < 4) {
				for (int i = start; i < start + length; i++) {
					results[i] = profiles[i].score(document);
				}
			} else if (length > 1) {
				int split = length / 2;
				invokeAll(new Scoring(profiles, document, start, split, results),
						  new Scoring(profiles, document, start + split, length - split, results));
			}
		}

	}
}