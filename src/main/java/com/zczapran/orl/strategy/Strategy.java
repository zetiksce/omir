package com.zczapran.orl.strategy;

import com.zczapran.orl.document.Document;
import com.zczapran.orl.profile.Profile;

/**
 * Interface for document-profile strategies.
 * 
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public interface Strategy<T extends Document, V extends Profile<T>> {

}
