package com.zczapran.orl.profile.multiinterest;

import com.zczapran.orl.document.Document;
import com.zczapran.orl.profile.Profile;
import com.zczapran.orl.strategy.adddocument.AddDocumentStrategy;
import com.zczapran.orl.strategy.scoredocument.ScoreDocumentStrategy;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class MultiInterestProfile<T extends Document, V extends Profile<T>> implements Profile<T> {

	private List<V> profiles = new ArrayList<>();
	private final AddDocumentStrategy<T, V> addDocumentStrategy;
	private final ScoreDocumentStrategy<T, V> scoreDocumentStrategy;
	private int size = 0;

	public MultiInterestProfile(AddDocumentStrategy<T, V> addDocumentStrategy,
			ScoreDocumentStrategy<T, V> scoreDocumentStrategy) {
		this.addDocumentStrategy = addDocumentStrategy;
		this.scoreDocumentStrategy = scoreDocumentStrategy;
	}

	@Override
	public void add(T document) {
		addDocumentStrategy.add(profiles, document);
		size++;
	}

	@Override
	public double score(T document) {
		return scoreDocumentStrategy.score(profiles, document);
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append(this.getClass().getName()).append('(');
		sb.append(addDocumentStrategy.getClass().getName()).append(',');
		sb.append(scoreDocumentStrategy.getClass().getName()).append(')').append('\n');

		for (V profile : profiles) {
			sb.append(' ').append(profile.size()).append(' ');
		}
		sb.append('\n');

		return sb.toString();
	}

}