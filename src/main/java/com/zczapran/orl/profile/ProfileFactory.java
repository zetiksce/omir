package com.zczapran.orl.profile;

import com.zczapran.orl.document.Document;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public interface ProfileFactory<V extends Profile> {

	/**
	 * Creates and returns a new profile.
	 *
	 * @return Object of type V that represents new profile.
	 */
	public V getProfile();

}
