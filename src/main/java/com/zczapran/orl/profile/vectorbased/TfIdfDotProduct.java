package com.zczapran.orl.profile.vectorbased;

import com.zczapran.orl.profile.vectorbased.corpus.Corpus;
import com.zczapran.orl.util.operation.DotProduct;
import java.util.Map;

/**
 * Special implementation of DotProduct for calculating dot-product of term vectors.
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class TfIdfDotProduct implements DotProduct<TermVector> {

	/**
	 * Corpus.
	 */
	private final Corpus corpus;

	/**
	 * Constructor.
	 *
	 * @param corpus Injected corpus.
	 */
	public TfIdfDotProduct(Corpus corpus) {
		this.corpus = corpus;
	}

	/**
	 * Calculates the dot-product between term vectors.
	 *
	 * @param a Instance of first term vector
	 * @param b Instance of second term vector
	 *
	 * @return Dot-product for two given vectors
	 */
	@Override
	public double calculate(TermVector a, TermVector b) {
		double dotProduct = 0.0;

		for (Map.Entry<Integer, Integer> entry : a) {
			int key = entry.getKey();
			double idf = corpus.getInvertedDocumentFrequency(key);
			double wa = a.getTermFrequency(key) * idf;
			double wb = b.getTermFrequency(key) * idf;
			dotProduct += wa * wb;
		}

		return dotProduct;
	}
}
