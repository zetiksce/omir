package com.zczapran.orl.profile.vectorbased.weightingscheme;

import com.zczapran.orl.profile.vectorbased.TermVector;

/**
 * Natural Term Frequency weightning scheme.
 *
 * As described in Manning, Raghavan, Schutze, Introduction to Information
 * Retrieval, Cambridge University Press, 2008.
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class NaturalTermFrequency implements TermFrequencyScheme {

	@Override
	public double getWeight(TermVector vector, int index) {
		return (double)vector.getTermCount(index);
	}

}
