package com.zczapran.orl.profile.vectorbased.weightingscheme;

import com.zczapran.orl.profile.vectorbased.corpus.Corpus;

/**
 * Interface for document frequency weightning schemes.
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public interface DocumentFrequencyScheme extends WeightingScheme<Corpus> {

}
