package com.zczapran.orl.profile.vectorbased.dictionary;

import java.util.HashMap;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class HashMapDictionary implements Dictionary {

	final private HashMap<String, Integer> dict;
	private int counter = 0;

	public HashMapDictionary() {
		dict = new HashMap<>();
	}

	@Override
	public Integer lookup(String concept) {
		return dict.get(concept);
	}

	@Override
	public boolean add(String concept) {
		if (!dict.containsKey(concept)) {
			dict.put(concept, counter);
			counter++;
			return true;
		}
		return false;
	}

	@Override
	public int size() {
		return counter;
	}

}
