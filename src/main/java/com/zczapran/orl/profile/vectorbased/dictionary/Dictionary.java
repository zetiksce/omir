package com.zczapran.orl.profile.vectorbased.dictionary;

/**
 * Interface for concept dictionaries.
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public interface Dictionary {

	/**
	 * Looks up concept in the dictionary.
	 *
	 * @param concept String representation of the concept.
	 *
	 * @return null if concept not present, otherwise index of the concept in the dictionary
	 */
	public Integer lookup(String concept);

	/**
	 * Adds a concept to the dictionary. Ignores if concept already in it.
	 *
	 * @param concept String representation of the concept.
	 *
	 * @return True if new concept added, False if concept was already in the dictionary.
	 */
	public boolean add(String concept);

	/**
	 * Gets sie of the dictionary.
	 *
	 * @return Number of concepts in the dictionary.
	 */
	public int size();
	
}
