package com.zczapran.orl.profile.vectorbased;

import com.zczapran.orl.document.Document;
import com.zczapran.orl.profile.vectorbased.weightingscheme.TermFrequencyScheme;
import com.zczapran.orl.util.vector.sparse.SparseVector;
import java.util.Iterator;
import java.util.Map.Entry;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class VectorDocument implements Document, TermVector {

	private final String uri;
	private final String content;
	private final SparseVector vector;
	private final TermFrequencyScheme termFrequencyScheme;

	public VectorDocument(String uri, String content, SparseVector vector, TermFrequencyScheme termFrequencyScheme) {
		this.uri = uri;
		this.content = content;
		this.vector = vector;
		this.termFrequencyScheme = termFrequencyScheme;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getContent() {
		return content;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getUri() {
		return uri;
	}

	public SparseVector getVector() {
		return vector;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getTermCount(int index) {
		return vector.get(index);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public double getTermFrequency(int index) {
		return termFrequencyScheme.getWeight(this, index);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Iterator<Entry<Integer, Integer>> iterator() {
		return vector.iterator();
	}

	@Override
	public int getMaxTermCount() {
		return vector.getMax();
	}

}