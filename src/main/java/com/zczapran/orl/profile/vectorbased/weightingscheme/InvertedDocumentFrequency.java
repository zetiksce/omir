package com.zczapran.orl.profile.vectorbased.weightingscheme;

import com.zczapran.orl.profile.vectorbased.corpus.Corpus;

/**
 * Inverted Document Frequency weightning scheme.
 *
 * As described in Manning, Raghavan, Schutze, Introduction to Information
 * Retrieval, Cambridge University Press, 2008.
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class InvertedDocumentFrequency implements DocumentFrequencyScheme {

	@Override
	public double getWeight(Corpus vector, int index) {
		if (vector.getDocumentCount(index) == 0) {
			throw new RuntimeException("Index " + index + " not in the corpus.");
		}
		return Math.log(vector.size() / (double)vector.getDocumentCount(index));
	}

}
