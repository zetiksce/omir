package com.zczapran.orl.profile.vectorbased.weightingscheme;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public interface WeightingScheme<T> {

	public double getWeight(T vector, int index);

}
