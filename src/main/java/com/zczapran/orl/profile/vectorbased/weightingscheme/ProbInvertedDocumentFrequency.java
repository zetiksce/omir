package com.zczapran.orl.profile.vectorbased.weightingscheme;

import com.zczapran.orl.profile.vectorbased.corpus.Corpus;

/**
 * Prob Inverted Document Frequency weightning scheme.
 *
 * As described in Manning, Raghavan, Schutze, Introduction to Information
 * Retrieval, Cambridge University Press, 2008.
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class ProbInvertedDocumentFrequency implements DocumentFrequencyScheme {

	@Override
	public double getWeight(Corpus vector, int index) {
		int df = vector.getDocumentCount(index);
		if (df == 0) {
			throw new RuntimeException("Index " + index + " not in the corpus.");
		} else {
			return Math.max(0.0, Math.log((vector.size() - df) / (double) df));
		}
	}

}
