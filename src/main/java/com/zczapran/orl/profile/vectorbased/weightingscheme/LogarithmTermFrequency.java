package com.zczapran.orl.profile.vectorbased.weightingscheme;

import com.zczapran.orl.profile.vectorbased.TermVector;

/**
 * Logarithm Term Frequency weightning scheme.
 *
 * As described in Manning, Raghavan, Schutze, Introduction to Information
 * Retrieval, Cambridge University Press, 2008.
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class LogarithmTermFrequency implements TermFrequencyScheme {

	@Override
	public double getWeight(TermVector vector, int index) {
		int cnt = vector.getTermCount(index);
		if (cnt > 0) {
			return 1.0 + Math.log(vector.getTermCount(index));
		} else {
			return 0.0;
		}
	}

}
