package com.zczapran.orl.profile.vectorbased.weightingscheme;

import com.zczapran.orl.profile.vectorbased.TermVector;

/**
 * Interface for term frequency weightning schemes.
 * 
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public interface TermFrequencyScheme extends WeightingScheme<TermVector> {

}