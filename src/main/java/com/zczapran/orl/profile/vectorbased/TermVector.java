package com.zczapran.orl.profile.vectorbased;

import java.util.Map;

/**
 * Interface for term vectors.
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public interface TermVector extends Iterable<Map.Entry<Integer, Integer>> {

	/**
	 * Gets number of occurrences of given term in the vector.
	 *
	 * @param index Term index in the corpus/vector
	 *
	 * @return 0 if no occurrences, otherwise number of occurrences
	 */
	public int getTermCount(int index);

	/**
	 * Gets maxiumum number of occurrences of a single term in the vector.
	 *
	 * @return 0 if no terms in documents
	 */
	public int getMaxTermCount();

	/**
	 * Gets the term frequency (TF) of given term in the vector.
	 *
	 * @param index Term index in the corpus/vector
	 *
	 * @return Value of the term frequency
	 */
	public double getTermFrequency(int index);

}
