package com.zczapran.orl.profile.vectorbased.corpus;

import com.zczapran.orl.profile.vectorbased.VectorDocument;
import com.zczapran.orl.profile.vectorbased.dictionary.Dictionary;
import com.zczapran.orl.profile.vectorbased.weightingscheme.DocumentFrequencyScheme;
import com.zczapran.orl.util.vector.ArrayListVector;
import java.util.HashSet;
import java.util.Map.Entry;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class ArrayListCorpus implements Corpus {

	final private ArrayListVector counts;
	final private HashSet<String> documentUris = new HashSet<>();
	private final Dictionary dictionary;
	private final DocumentFrequencyScheme documentFrequency;

	public ArrayListCorpus(Dictionary dictionary, DocumentFrequencyScheme documentFrequency) {
		this.dictionary = dictionary;
		this.counts = new ArrayListVector(dictionary.size());
		this.documentFrequency = documentFrequency;
	}

	@Override
	public boolean add(VectorDocument document) {
		if (!documentUris.contains(document.getUri())) {
			documentUris.add(document.getUri());
			for (Entry<Integer, Integer> entry : document.getVector()) {
				Integer index = entry.getKey();
				Integer value = entry.getValue();
				if (index < counts.size()) {
					counts.put(index, counts.get(index) + 1);
				} else {
					counts.put(index, value);
				}
			}
			return true;
		}
		return false;
	}

	@Override
	public int getDocumentCount(String concept) {
		Integer index = dictionary.lookup(concept);
		return getDocumentCount(index);
	}

	@Override
	public int getDocumentCount(int index) {
		return counts.get(index);
	}

	@Override
	public int size() {
		return documentUris.size();
	}

	@Override
	public double getInvertedDocumentFrequency(int index) {
		return documentFrequency.getWeight(this, index);
	}

}
