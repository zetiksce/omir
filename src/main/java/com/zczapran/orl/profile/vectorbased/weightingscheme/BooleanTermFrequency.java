package com.zczapran.orl.profile.vectorbased.weightingscheme;

import com.zczapran.orl.profile.vectorbased.TermVector;

/**
 * Boolean Term Frequency weightning scheme.
 *
 * As described in Manning, Raghavan, Schutze, Introduction to Information
 * Retrieval, Cambridge University Press, 2008.
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class BooleanTermFrequency implements TermFrequencyScheme {

	@Override
	public double getWeight(TermVector vector, int index) {
		return vector.getTermCount(index) > 0 ? 1.0 : 0.0;
	}

}
