package com.zczapran.orl.profile.vectorbased;

import com.zczapran.orl.profile.ProfileFactory;
import com.zczapran.orl.profile.vectorbased.weightingscheme.TermFrequencyScheme;
import com.zczapran.orl.util.Similarity;

/**
 * Factory for VectorBasedProfile objects.
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class VectorBasedProfileFactory implements ProfileFactory<VectorBasedProfile> {

	/**
	 * Ontology that is passed to the created profiles.
	 */
	private final Similarity similarity;
	private final TermFrequencyScheme termFrequencyScheme;

	/**
	 * Standard constructor.
	 *
	 * @param similarity Similarity to use while creating profile.
	 */
	public VectorBasedProfileFactory(Similarity similarity, TermFrequencyScheme termFrequencyScheme) {
		this.similarity = similarity;
		this.termFrequencyScheme = termFrequencyScheme;
	}

	/**
	 * Creates and returns a new profile.
	 *
	 * @return Object of type VectorBasedProfile that represents new profile.
	 */
	@Override
	public VectorBasedProfile getProfile() {
		return new VectorBasedProfile(similarity, termFrequencyScheme);
	}

}
