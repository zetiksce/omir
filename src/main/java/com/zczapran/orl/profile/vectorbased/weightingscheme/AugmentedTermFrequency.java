package com.zczapran.orl.profile.vectorbased.weightingscheme;

import com.zczapran.orl.profile.vectorbased.TermVector;

/**
 * Boolean Term Frequency weightning scheme.
 *
 * As described in Manning, Raghavan, Schutze, Introduction to Information
 * Retrieval, Cambridge University Press, 2008.
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class AugmentedTermFrequency implements TermFrequencyScheme {

	@Override
	public double getWeight(TermVector vector, int index) {
		int max = vector.getMaxTermCount();
		return 0.5 + (0.5 * vector.getTermCount(index) / max);
	}

}
