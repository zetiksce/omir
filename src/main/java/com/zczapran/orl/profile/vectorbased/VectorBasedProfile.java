package com.zczapran.orl.profile.vectorbased;

import com.zczapran.orl.profile.Mergable;
import com.zczapran.orl.profile.Scorable;
import com.zczapran.orl.profile.vectorbased.weightingscheme.TermFrequencyScheme;
import com.zczapran.orl.util.Similarity;
import com.zczapran.orl.util.vector.sparse.SparseVector;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;

/**
 * Implementation of a vector-based document recommendation profile.
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class VectorBasedProfile implements Mergable<VectorDocument, VectorBasedProfile>, Scorable<VectorDocument, VectorBasedProfile>, TermVector {

	/**
	 * Set of all document uris added to the profile.
	 */
	protected final HashSet<String> documentUris = new HashSet<>();

	/**
	 * Underlying vector representation of the profile.
	 */
	protected final SparseVector vector = new SparseVector();

	/**
	 * Similarity measure used for term vector comparison.
	 */
	private final Similarity<TermVector> similarity;

	/**
	 * Term frequency weighting scheme.
	 */
	private final TermFrequencyScheme termFrequencyScheme;

	/**
	 * Constructor.
	 *
	 * @param similarity Similarity measure used for term vector comparison
	 * @param termFrequencyScheme Term frequency weighting scheme
	 */
	public VectorBasedProfile(Similarity<TermVector> similarity, TermFrequencyScheme termFrequencyScheme) {
		this.similarity = similarity;
		this.termFrequencyScheme = termFrequencyScheme;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void add(VectorDocument document) {
		if (documentUris.add(document.getUri())) {
			vector.add(document.getVector());
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public double score(VectorDocument document) {
		return similarity.between(this, document);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int size() {
		return documentUris.size();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void merge(VectorBasedProfile profile) {
		vector.add(profile.vector);
		documentUris.addAll(profile.documentUris);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public double score(VectorBasedProfile profile) {
		return similarity.between(this, profile);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getTermCount(int index) {
		return vector.get(index);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public double getTermFrequency(int index) {
		return termFrequencyScheme.getWeight(this, index);
	}



	/**
	 * {@inheritDoc}
	 */
	@Override
	public Iterator<Entry<Integer, Integer>> iterator() {
		return vector.iterator();
	}

	@Override
	public int getMaxTermCount() {
		return vector.getMax();
	}

}