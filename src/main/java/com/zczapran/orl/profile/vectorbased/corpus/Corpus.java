package com.zczapran.orl.profile.vectorbased.corpus;

import com.zczapran.orl.profile.vectorbased.VectorDocument;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public interface Corpus {

	public boolean add(VectorDocument document);

	/**
	 * Total number of documents in corpus.
	 *
	 * @return Integer value greater or equal to zero
	 */
	public int size();

	public int getDocumentCount(String concept);

	public int getDocumentCount(int index);

	public double getInvertedDocumentFrequency(int index);
}
