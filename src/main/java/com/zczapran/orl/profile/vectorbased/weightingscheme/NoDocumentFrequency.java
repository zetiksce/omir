package com.zczapran.orl.profile.vectorbased.weightingscheme;

import com.zczapran.orl.profile.vectorbased.corpus.Corpus;

/**
 * Inverted Document Frequency weightning scheme.
 *
 * As described in Manning, Raghavan, Schutze, Introduction to Information
 * Retrieval, Cambridge University Press, 2008.
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class NoDocumentFrequency implements DocumentFrequencyScheme {

	@Override
	public double getWeight(Corpus vector, int index) {
		return 1.0;
	}

}
