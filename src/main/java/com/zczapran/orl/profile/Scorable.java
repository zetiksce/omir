package com.zczapran.orl.profile;

import com.zczapran.orl.document.Document;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public interface Scorable<T extends Document, V extends Profile<T>> extends Profile<T> {

	public double score(V profile);

}