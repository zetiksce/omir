package com.zczapran.orl.profile;

import com.zczapran.orl.document.Document;

/**
 * Interface for a profile.
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public interface Profile<T extends Document> {

	/**
	 * Adds a document to the profile.
	 *
	 * @param document A document.
	 */
	public void add(T document);

	/**
	 * Gets the score for a document.
	 *
	 * @param document
	 *
	 * @return Calculated score for a document and profile. Usually normalized.
	 */
	public double score(T document);

	/**
	 * Gets the size (number of all documents).
	 *
	 * @return Number of all documents or different measure of size
	 */
	public int size();

}
