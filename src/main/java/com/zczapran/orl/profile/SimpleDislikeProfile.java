package com.zczapran.orl.profile;

import com.zczapran.orl.document.Document;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class SimpleDislikeProfile<T extends Document> implements DislikeProfile<T> {

	private final Profile<T> likeProfile;
	private final Profile<T> dislikeProfile;

	public SimpleDislikeProfile(Profile<T> likeProfile, Profile<T> dislikeProfile) {
		this.likeProfile = likeProfile;
		this.dislikeProfile = dislikeProfile;
	}

	@Override
	public void add(T document) {
		likeProfile.add(document);
	}

	@Override
	public double score(T document) {
		return (1 + likeProfile.score(document) - dislikeProfile.score(document)) / 2;
	}

	@Override
	public void dislike(T document) {
		dislikeProfile.add(document);
	}

	@Override
	public int size() {
		return likeProfile.size() + dislikeProfile.size();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("SimpleDislikeProfile:\n");
		sb.append(likeProfile);
		sb.append(dislikeProfile);
		return sb.toString();
	}
}
