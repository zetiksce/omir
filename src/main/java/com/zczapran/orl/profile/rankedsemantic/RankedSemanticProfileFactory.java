package com.zczapran.orl.profile.rankedsemantic;

import com.zczapran.orl.ontology.Ontology;
import com.zczapran.orl.profile.ProfileFactory;

/**
 * Factory for RankedSemanticProfile objects.
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class RankedSemanticProfileFactory implements ProfileFactory<RankedSemanticProfile> {

	/**
	 * Ontology that is passed to the created profiles.
	 */
	private final Ontology ontology;

	/**
	 * Standard constructor.
	 *
	 * @param ontology Ontology to use while creating profile.
	 */
	public RankedSemanticProfileFactory(Ontology ontology) {
		this.ontology = ontology;
	}

	/**
	 * Creates and returns a new profile.
	 *
	 * @return Object of type RankedSemanticProfile that represents new profile.
	 */
	@Override
	public RankedSemanticProfile getProfile() {
		return new RankedSemanticProfile(ontology);
	}

}
