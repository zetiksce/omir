package com.zczapran.orl.profile;

import com.zczapran.orl.document.Document;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public interface DislikeProfile<T extends Document> extends Profile<T> {

	public void dislike(T document);

}
