package com.zczapran.orl.profile.weighted;

import com.zczapran.orl.document.Document;
import com.zczapran.orl.profile.Profile;

/**
 * Decorator class that extends functionality of a profile by implementing
 * Weighted interface.
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class WeightedDecorator<T extends Document, V extends Profile<T>> implements Weighted<T> {

	private final V subprofile;
	private final double step;
	private double weight;

	/**
	 * Constructor with default step value (1.0).
	 *
	 * @param subprofile Profile being decorated.
	 * @param weight Starting weight of the profile.
	 */
	public WeightedDecorator(V subprofile, double weight) {
		this.subprofile = subprofile;
		this.step = 1.0;
		this.weight = weight;
	}

	/**
	 * Constructor that allows to overwrite default step value.
	 *
	 * @param subprofile
	 * @param weight
	 * @param step Any double value greater than zero.
	 */
	public WeightedDecorator(V subprofile, double weight, double step) {
		this.subprofile = subprofile;
		this.weight = weight;
		this.step = step;
	}

	@Override
	public double weight() {
		return weight;
	}

	@Override
	public void add(T document) {
		subprofile.add(document);
		weight += step;
	}

	@Override
	public double score(T document) {
		return subprofile.score(document);
	}

	@Override
	public int size() {
		return subprofile.size();
	}

	@Override
	public void increaseWeight(double value) {
		weight += value;
	}

	/**
	 * Gets decorated profile.
	 *
	 * @return Profile being decorated.
	 */
	protected V getSubprofile() {
		return subprofile;
	}

}
