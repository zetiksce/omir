package com.zczapran.orl.profile.weighted;

import com.zczapran.orl.document.Document;
import com.zczapran.orl.profile.Mergable;
import com.zczapran.orl.profile.ProfileFactory;
import com.zczapran.orl.profile.Scorable;

/**
 * Factory for MergableAndScorableWeightedDecorator profiles.
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class MergableAndScorableWeightedDecoratorFactory<T extends Document, V extends Mergable<T, V> & Scorable<T, V>>
		implements ProfileFactory<MergableAndScorableWeightedDecorator<T, V>> {
	private final ProfileFactory<V> subfactory;
	private final double initWeight;
	private final double step;

	public MergableAndScorableWeightedDecoratorFactory(ProfileFactory<V> subfactory, double initWeight, double step) {
		this.subfactory = subfactory;
		this.initWeight = initWeight;
		this.step = step;
	}

	@Override
	public MergableAndScorableWeightedDecorator<T, V> getProfile() {
		return new MergableAndScorableWeightedDecorator<>(subfactory.getProfile(), initWeight, step);
	}

}
