package com.zczapran.orl.profile.weighted;

import com.zczapran.orl.document.Document;
import com.zczapran.orl.profile.Profile;

/**
 * Interface for weighted profiles.
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public interface Weighted<T extends Document> extends Profile<T> {

	/**
	 * Gets current weight of the profile.
	 *
	 * @return Current weight
	 */
	public double weight();

	/**
	 * Increases weight of the profile.
	 *
	 * Allows subzero arguments if need to decrease.
	 *
	 * @param value Any double value
	 */
	public void increaseWeight(double value);

}
