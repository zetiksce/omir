package com.zczapran.orl.profile.weighted;

import com.zczapran.orl.document.Document;
import com.zczapran.orl.profile.Mergable;
import com.zczapran.orl.profile.Scorable;

/**
 * Allows merging to weighted profiles.
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class MergableAndScorableWeightedDecorator<T extends Document, V extends Mergable<T, V> & Scorable<T, V>>
		extends WeightedDecorator<T, V>
		implements Mergable<T, MergableAndScorableWeightedDecorator<T, V>>, Scorable<T, MergableAndScorableWeightedDecorator<T, V>> {

	public MergableAndScorableWeightedDecorator(V subprofile, double initWeight) {
		super(subprofile, initWeight);
	}

	public MergableAndScorableWeightedDecorator(V subprofile, double initWeight, double step) {
		super(subprofile, initWeight, step);
	}

	@Override
	public void merge(MergableAndScorableWeightedDecorator<T, V> profile) {
		getSubprofile().merge(profile.getSubprofile());
		increaseWeight(profile.weight());
	}

	@Override
	public double score(MergableAndScorableWeightedDecorator<T, V> profile) {
		return getSubprofile().score(profile.getSubprofile());
	}

}
