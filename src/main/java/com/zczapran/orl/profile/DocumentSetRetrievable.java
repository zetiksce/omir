package com.zczapran.orl.profile;

import com.zczapran.orl.document.Document;
import java.util.Set;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public interface DocumentSetRetrievable<T extends Document> extends Profile<T> {

	/**
	 * Gets collection of documents in the profile.
	 *
	 * @return
	 */
	public Set<T> documents();

}
