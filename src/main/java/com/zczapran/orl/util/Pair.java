package com.zczapran.orl.util;

import java.util.Objects;

/**
 * Immutable pair of objects.
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class Pair<T, V> {

	private final T left;
	private final V right;

	/**
	 * Standard constructor.
	 *
	 * @param left
	 * @param right
	 */
	public Pair(T left, V right) {
		this.left = left;
		this.right = right;
	}

	/**
	 * Gets the left-value.
	 *
	 * @return
	 */
	public T left() {
		return left;
	}

	/**
	 * Gets the right-value.
	 *
	 * @return
	 */
	public V right() {
		return right;
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + "(" + left + ", " + right + ")";
	}

	@Override
	public boolean equals(Object o) {
		return this.getClass() == o.getClass()
				&& this.hashCode() == o.hashCode()
				&& this.left().equals(((Pair) o).left())
				&& this.right().equals(((Pair) o).right());
	}

	@Override
	public int hashCode() {
		int hash = 5;
		hash = 37 * hash + Objects.hashCode(this.left);
		hash = 37 * hash + Objects.hashCode(this.right);
		return hash;
	}

}
