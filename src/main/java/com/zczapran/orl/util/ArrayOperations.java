package com.zczapran.orl.util;

/**
 * Useful array operations.
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
final public class ArrayOperations {

	/**
	 * Finds the maximum value and its index.
	 *
	 * @param values
	 *
	 * @return
	 */
	public static Pair<Integer, Double> max(double[] values) {
		if (values.length < 1) {
			return new Pair<>(null, null);
		}

		double max = values[0];
		int index = 0;
		for (int i = 1; i < values.length; i++) {
			if (values[i] > max) {
				max = values[i];
				index = i;
			}
		}

		return new Pair<>(index, max);
	}

}
