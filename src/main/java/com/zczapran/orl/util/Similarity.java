package com.zczapran.orl.util;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public interface Similarity<T> {

	public double between(T a, T b);

}
