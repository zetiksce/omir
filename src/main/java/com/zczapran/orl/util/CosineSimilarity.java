package com.zczapran.orl.util;

import com.zczapran.orl.util.operation.DotProduct;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class CosineSimilarity<T> implements Similarity<T> {
	private final DotProduct<T> dotProduct;

	public CosineSimilarity(DotProduct<T> dotProduct) {
		this.dotProduct = dotProduct;
	}

	@Override
	public double between(T a, T b) {
		return dotProduct.calculate(a, b) / (Math.sqrt(dotProduct.calculate(a, a)) * Math.sqrt(dotProduct.calculate(b, b)));
	}

}