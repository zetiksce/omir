package com.zczapran.orl.util.vector.sparse;

import com.zczapran.orl.util.operation.DotProduct;
import java.util.Map.Entry;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class SparseVectorDotProduct implements DotProduct<SparseVector> {

	@Override
	public double calculate(SparseVector a, SparseVector b) {
		int dotProduct = 0;

		for (Entry<Integer, Integer> entry : a) {
			dotProduct += entry.getValue() * b.get(entry.getKey());
		}

		return (double)dotProduct;
	}

}
