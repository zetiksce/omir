package com.zczapran.orl.util.vector;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public interface Vector<T extends Number> {

	public T get(int index);

	public void put(int index, T value);

}