package com.zczapran.orl.util.vector.sparse;

import com.zczapran.orl.util.vector.Vector;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class SparseVector implements Vector<Integer>, Iterable<Entry<Integer, Integer>> {

	private final HashMap<Integer, Integer> map;

	private int max = 0;

	public SparseVector() {
		this.map = new HashMap<>();
	}

	@Override
	public Integer get(int index) {
		return map.containsKey(index) ? map.get(index) : 0;
	}

	public void add(SparseVector vector) {
		for (Entry<Integer, Integer> entry : vector) {
			int value = get(entry.getKey()) + entry.getValue();
			put(entry.getKey(), value);
			if (value > max) max = value;
		}
	}

	@Override
	public Iterator<Entry<Integer, Integer>> iterator() {
		return map.entrySet().iterator();
	}

	@Override
	public void put(int index, Integer value) {
		map.put(index, value);
		if (value > max) max = value;
	}

	public Set<Integer> keys() {
		return map.keySet();
	}

	public int getMax() {
		return max;
	}

}