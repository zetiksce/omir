package com.zczapran.orl.util.vector;

import java.util.ArrayList;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class ArrayListVector implements Vector<Integer> {

	private final ArrayList<Integer> list;

	public ArrayListVector() {
		this.list = new ArrayList<>();
	}

	public ArrayListVector(int size) {
		this.list = new ArrayList<>(size);
		put(size - 1, 0);
	}

	@Override
	public Integer get(int index) {
		if (size() <= index || index < 0) {
			throw new IndexOutOfBoundsException();
		}
		Integer value = list.get(index);
		return value == null ? 0 : value;
	}

	@Override
	public void put(int index, Integer value) {
		while (list.size() <= index) {
			list.add(0);
		}
		list.set(index, value);
	}

	public int size() {
		return list.size();
	}

}
