package com.zczapran.orl.util.operation;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public interface DotProduct<T> {

	public double calculate(T a, T b);

}
