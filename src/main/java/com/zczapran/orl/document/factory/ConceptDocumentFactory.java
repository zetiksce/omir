package com.zczapran.orl.document.factory;

import com.zczapran.orl.analyser.Analyser;
import com.zczapran.orl.document.ConceptDocument;
import java.util.Collection;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
abstract public class ConceptDocumentFactory<T extends ConceptDocument> implements DocumentFactory<T> {

	private Analyser analyzer;

	public ConceptDocumentFactory(Analyser analyzer) {
		this.analyzer = analyzer;
	}

	protected Collection<String> getConcepts(String content) {
		return analyzer.analyse(content);
	}

}
