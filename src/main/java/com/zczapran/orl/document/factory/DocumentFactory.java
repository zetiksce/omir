package com.zczapran.orl.document.factory;

import com.zczapran.orl.document.Document;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public interface DocumentFactory<T extends Document> {

	public T getDocument(String uri, String content);

}
