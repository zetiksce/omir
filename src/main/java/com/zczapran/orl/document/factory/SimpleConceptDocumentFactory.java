package com.zczapran.orl.document.factory;

import com.zczapran.orl.analyser.Analyser;
import com.zczapran.orl.document.ConceptDocument;
import com.zczapran.orl.document.SimpleConceptDocument;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class SimpleConceptDocumentFactory extends ConceptDocumentFactory<ConceptDocument> {

	public SimpleConceptDocumentFactory(Analyser analyser) {
		super(analyser);
	}

	@Override
	public SimpleConceptDocument getDocument(String uri, String content) {
		return new SimpleConceptDocument(uri, this.getConcepts(content));
	}

}
