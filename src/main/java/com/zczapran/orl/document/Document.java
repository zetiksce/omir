package com.zczapran.orl.document;

/**
 * Interface for documents.
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public interface Document {

	/**
	 * Gets the content.
	 *
	 * @param content
	 *
	 * @return Textual representation of the content.
	 */
	public String getContent();

	/**
	 * Gets the uri.
	 *
	 * @return
	 */
	public String getUri();

}
