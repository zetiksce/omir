package com.zczapran.orl.document;

import java.util.Collection;

/**
 * Interface for concept documents.
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public interface ConceptDocument extends Document {

	/**
	 * Gets set of concepts in the document.
	 *
	 * @return Sets of concepts in the document.
	 */
	public Collection<String> getConcepts();

}
