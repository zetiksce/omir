package com.zczapran.orl.document;

import java.util.Arrays;
import java.util.Collection;

/**
 * Simple class that represents a concept document.
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class SimpleConceptDocument implements ConceptDocument {

	final private String uri;
	final private Collection<String> concepts;

	public SimpleConceptDocument(String uri, Collection<String> concepts) {
		this.uri = uri;
		this.concepts = concepts;
	}

	public SimpleConceptDocument(String uri, String[] concepts) {
		this(uri, Arrays.asList(concepts));
	}

	@Override
	public Collection<String> getConcepts() {
		return this.concepts;
	}

	@Override
	public String getContent() {
		StringBuilder sb = new StringBuilder();
		for (String concept : this.concepts) {
			sb.append(concept).append(' ');
		}
		return sb.toString();
	}

	@Override
	public String getUri() {
		return uri;
	}

}
