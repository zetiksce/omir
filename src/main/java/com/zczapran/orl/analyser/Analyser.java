package com.zczapran.orl.analyser;

import java.util.List;

/**
 * Interface for text analysers.
 *
 * Converts text into list of terms.
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public interface Analyser {

	/**
	 * Converts content into list of terms.
	 *
	 * @param content Raw text contennt
	 * @return List of terms
	 */
	public List<String> analyse(String content);

}
