package com.zczapran.orl.util.vector;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class ArrayListVectorTest {

	private ArrayListVector object;

	@Rule
	public ExpectedException exception = ExpectedException.none();


	@Before
	public void setUp() {
		object = new ArrayListVector(2);
	}

	@Test
	public void testNoSizeInConstructor() {
		object = new ArrayListVector();
		assertEquals(0, object.size());
	}

	/**
	 * Test of get method, of class ArrayListVector.
	 */
	@Test
	public void testGet() {
		assertEquals(0, object.get(0).intValue());
		exception.expect(IndexOutOfBoundsException.class);
		object.get(6).intValue();
	}

	/**
	 * Test of put method, of class ArrayListVector.
	 */
	@Test
	public void testPut() {
		object.put(1, 4);
		assertEquals(4, object.get(1).intValue());
		assertEquals(2, object.size());
		object.put(6, 2);
		assertEquals(2, object.get(6).intValue());
		assertEquals(7, object.size());
	}

	/**
	 * Test of size method, of class ArrayListVector.
	 */
	@Test
	public void testSize() {
		assertEquals(2, object.size());
	}

}
