package com.zczapran.orl.util;

import com.zczapran.orl.util.operation.DotProduct;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Test for CosineSimilarity.
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class CosineSimilarityTest {

	/**
	 * Test of between method, of class CosineSimilarity.
	 */
	@Test
	public void testBetween() {
		DotProduct dotProduct = mock(DotProduct.class);
		CosineSimilarity object = new CosineSimilarity(dotProduct);
		Object a = mock(Object.class);
		Object b = mock(Object.class);
		when(dotProduct.calculate(a, b)).thenReturn(4.0);
		when(dotProduct.calculate(a, a)).thenReturn(9.0);
		when(dotProduct.calculate(b, b)).thenReturn(4.0);
		double result = object.between(a, b);
		assertEquals(0.66666, result, 0.0001);
	}

}
