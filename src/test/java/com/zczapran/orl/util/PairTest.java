package com.zczapran.orl.util;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

/**
 * Test for Pair.
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class PairTest {

	/**
	 * Test of left method, of class Pair.
	 */
	@Test
	public void testLeft() {
		Pair<Integer, Integer> pair = new Pair<>(4, 5);
		assertEquals(4, (int) pair.left());
	}

	/**
	 * Test of right method, of class Pair.
	 */
	@Test
	public void testRight() {
		Pair<Integer, Integer> pair = new Pair<>(4, 5);
		assertEquals(5, (int) pair.right());
	}

	/**
	 * Test of toString method, of class Pair.
	 */
	@Test
	public void testToString() {
		Pair<Integer, Integer> pair = new Pair<>(4, 5);
		assertEquals("Pair(4, 5)", pair.toString());
	}

	/**
	 * Test of equals method, of class Pair.
	 */
	@Test
	public void testEquals() {
		Pair<Integer, Integer> pairA = new Pair<>(4, 5);
		Pair<Integer, Integer> pairB = new Pair<>(4, 5);
		assertTrue(pairA.equals(pairB));

		Pair<Integer, Integer> pairC = new Pair<>(5, 5);
		assertFalse(pairA.equals(pairC));

		Object object = mock(Object.class);
		assertFalse(pairA.equals(object));
	}

	/**
	 * Test of hashCode method, of class Pair.
	 */
	@Test
	public void testHashCode() {
		Pair<Integer, Integer> pairA = new Pair<>(4, 5);
		Pair<Integer, Integer> pairB = new Pair<>(4, 5);
		assertEquals(pairA.hashCode(), pairB.hashCode());
	}

}
