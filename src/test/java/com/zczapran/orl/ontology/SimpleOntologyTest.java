package com.zczapran.orl.ontology;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class SimpleOntologyTest {

	private SimpleOntology instance;

	public SimpleOntologyTest() {
	}

	@Before
	public void setUp() {
		this.instance = new SimpleOntology();
	}

	/**
	 * Test of addRelation method, of class SimpleOntology.
	 */
	@Test
	public void testAddRelation() {
		this.instance.addRelation("a", "b");
		this.instance.addRelation("c", "b");
		this.instance.addRelation("c", "a");
		Set<String> expectedConcepts = new HashSet<>();
		expectedConcepts.add("a");
		expectedConcepts.add("b");
		expectedConcepts.add("c");

		Set<String> concepts = this.instance.getConcepts();
		assertEquals(expectedConcepts, concepts);
		assertEquals(Arrays.asList(new String[]{"b"}), this.instance.getRelated("a"));
		assertEquals(Arrays.asList(new String[]{}), this.instance.getRelated("b"));
		assertEquals(Arrays.asList(new String[]{"b", "a"}), this.instance.getRelated("c"));
	}
}
