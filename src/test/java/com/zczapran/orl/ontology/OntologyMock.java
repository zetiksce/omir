package com.zczapran.orl.ontology;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class OntologyMock extends SimpleOntology {

	public OntologyMock() {
		super();
		this.addRelation("yahoo", "google");
		this.addRelation("yahoo", "apple");
		this.addRelation("google", "yahoo");
		this.addRelation("google", "apple");
		this.addRelation("apple", "yahoo");
		this.addRelation("apple", "google");
		this.addRelation("toyota", "prius");
		this.addRelation("obama", "usa");
		this.addRelation("china", "usa");
	}

}
