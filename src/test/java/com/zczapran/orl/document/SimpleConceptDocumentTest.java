package com.zczapran.orl.document;

import java.util.Arrays;
import java.util.Collection;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class SimpleConceptDocumentTest {

	/**
	 * Test of getConcepts method, of class SimpleConceptDocument.
	 */
	@Test
	public void testGetConcepts() {
		SimpleConceptDocument document;

		Collection<String> concepts = Arrays.asList(new String[]{"a", "b"});
		document = new SimpleConceptDocument("uri1", new String[]{"a", "b"});
		assertEquals(concepts, document.getConcepts());

		document = new SimpleConceptDocument("uri2", concepts);
		assertEquals(concepts, document.getConcepts());

		String content = document.getContent();
		for (String concept : concepts) {
			assertTrue(content.contains(concept));
		}
	}

	/**
	 * Test of getUri method, of class SimpleConceptDocument.
	 */
	@Test
	public void testGetUri() {
		String uri = "uri1";
		SimpleConceptDocument document = new SimpleConceptDocument(uri, new String[]{"a", "b"});
		assertEquals(uri, document.getUri());
	}

}