/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zczapran.orl.document.factory;

import com.zczapran.orl.analyser.Analyser;
import com.zczapran.orl.document.ConceptDocument;
import com.zczapran.orl.document.SimpleConceptDocument;
import java.util.Arrays;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.*;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class SimpleConceptDocumentFactoryTest {

	/**
	 * Test of getDocument method, of class SimpleConceptDocumentFactory.
	 */
	@Test
	public void testGetDocument() {
		Analyser analyser = mock(Analyser.class);
		ConceptDocumentFactory<ConceptDocument> factory = new SimpleConceptDocumentFactory(analyser);
		String uri1 = "uri1";
		String content = "a sample content.";
		String[] concepts = {"a", "sample", "content"};
		when(analyser.analyse(content)).thenReturn(Arrays.asList(concepts));
		ConceptDocument document = factory.getDocument(uri1, content);
		assertThat(document, instanceOf(SimpleConceptDocument.class));
		assertEquals(Arrays.asList(concepts), document.getConcepts());
	}

}
