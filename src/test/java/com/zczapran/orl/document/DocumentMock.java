package com.zczapran.orl.document;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class DocumentMock implements Document {

	final private String uri;
	private String content;

	public DocumentMock(String uri) {
		this.uri = uri;
	}

	@Override
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String getUri() {
		return uri;
	}

}
