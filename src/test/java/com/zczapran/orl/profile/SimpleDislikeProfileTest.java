package com.zczapran.orl.profile;

import com.zczapran.orl.document.Document;
import static org.hamcrest.CoreMatchers.instanceOf;
import org.junit.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class SimpleDislikeProfileTest {

	Profile<Document> likeProfile;
	Profile<Document> dislikeProfile;
	DislikeProfile<Document> object;

	@Before
	public void setUp() {
		likeProfile = mock(Profile.class);
		dislikeProfile = mock(Profile.class);
		object = new SimpleDislikeProfile<>(likeProfile, dislikeProfile);
	}

	/**
	 * Test of add method, of class SimpleDislikeProfile.
	 */
	@Test
	public void testAdd() {
		Document document = mock(Document.class);
		object.add(document);
		verify(likeProfile, times(1)).add(document);
		verify(dislikeProfile, times(0)).add(document);
	}

	/**
	 * Test of score method, of class SimpleDislikeProfile.
	 */
	@Test
	public void testScore() {
		Document document = mock(Document.class);
		when(likeProfile.score(document)).thenReturn(0.9);
		when(dislikeProfile.score(document)).thenReturn(0.3);
		assertEquals(0.8, object.score(document), 0.00001);
	}

	/**
	 * Test of dislike method, of class SimpleDislikeProfile.
	 */
	@Test
	public void testDislike() {
		Document document = mock(Document.class);
		object.dislike(document);
		verify(likeProfile, times(0)).add(document);
		verify(dislikeProfile, times(1)).add(document);
	}

	/**
	 * Test of size method, of class SimpleDislikeProfile.
	 */
	@Test
	public void testSize() {
		assertEquals(0, object.size());
		when(likeProfile.size()).thenReturn(3);
		when(dislikeProfile.size()).thenReturn(4);
		assertEquals(7, object.size());
	}

	/**
	 * Test of toString method, of class SimpleDislikeProfile.
	 */
	@Test
	public void testToString() {
		assertThat(object.toString(), instanceOf(String.class));
	}

}
