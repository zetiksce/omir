package com.zczapran.orl.profile;

import com.zczapran.orl.document.DocumentMock;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class MergableMock
		extends ProfileMock
		implements Mergable<DocumentMock, MergableMock> {

	@Override
	public void merge(MergableMock profile) {
		documents.addAll(profile.documents);
		scoredDocuments.putAll(profile.scoredDocuments);
	}

}
