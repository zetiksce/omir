package com.zczapran.orl.profile.rankedsemantic;

import com.zczapran.orl.document.ConceptDocument;
import com.zczapran.orl.document.SimpleConceptDocument;
import com.zczapran.orl.ontology.OntologyMock;
import com.zczapran.orl.ontology.SimpleOntology;
import gnu.trove.map.hash.TObjectDoubleHashMap;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 * Test class for RankedSemanticProfile.
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class RankedSemanticProfileTest {

	private RankedSemanticProfile instance;
	private SimpleOntology ontology;

	public RankedSemanticProfileTest() {
	}

	@Before
	public void setUp() {
		ontology = new OntologyMock();
		instance = new RankedSemanticProfile(ontology);
	}

	/**
	 * Test of add method, of class RankedSemanticProfile.
	 */
	@Test
	public void testAdd() {
		ConceptDocument document1 = new SimpleConceptDocument("uri1", new String[]{"yahoo", "obama", "china"});
		ConceptDocument document2 = new SimpleConceptDocument("uri2", new String[]{"yahoo", "obama", "china"});
		ConceptDocument document3 = new SimpleConceptDocument("uri3", new String[]{"yahoo", "obama"});
		ConceptDocument document4 = new SimpleConceptDocument("uri4", new String[]{"yahoo"});
		this.instance.add(document1);
		this.instance.add(document2);
		this.instance.add(document3);
		this.instance.add(document4);

		TObjectDoubleHashMap profile = this.instance.getInnerProfile(true);
		TObjectDoubleHashMap expected = new TObjectDoubleHashMap();
		expected.put("usa", 1.0);
		expected.put("yahoo", 0.90476190);
		expected.put("google", 0.61904761);
		expected.put("china", 0.0);
		expected.put("apple", 0.61904761);
		expected.put("obama", 0.45238095);

		for (Object concept : profile.keys()) {
			assertEquals(expected.get(concept), profile.get(concept), 0.0000001);
		}

		ConceptDocument document5 = new SimpleConceptDocument("uri5", new String[]{"google", "apple", "toyota"});
		assertEquals(0.596026490066, this.instance.score(document5), 0.0000001);
	}

	@Test
	public void testMerge()
	{
		ConceptDocument document1 = new SimpleConceptDocument("uri1", new String[]{"yahoo", "obama", "china"});
		ConceptDocument document2 = new SimpleConceptDocument("uri2", new String[]{"yahoo", "obama", "china"});
		ConceptDocument document3 = new SimpleConceptDocument("uri3", new String[]{"yahoo", "obama"});
		ConceptDocument document4 = new SimpleConceptDocument("uri4", new String[]{"yahoo"});

		RankedSemanticProfile instance2 = new RankedSemanticProfile(ontology);
		instance.add(document1);
		instance.add(document2);
		instance2.add(document3);
		instance2.add(document4);

		instance.merge(instance2);
		TObjectDoubleHashMap profile = this.instance.getInnerProfile(true);
		TObjectDoubleHashMap expected = new TObjectDoubleHashMap();
		expected.put("usa", 1.0);
		expected.put("yahoo", 0.90476190);
		expected.put("google", 0.61904761);
		expected.put("china", 0.0);
		expected.put("apple", 0.61904761);
		expected.put("obama", 0.45238095);

		for (Object concept : profile.keys()) {
			assertEquals(expected.get(concept), profile.get(concept), 0.0000001);
		}

		instance = new RankedSemanticProfile(ontology);
		instance2 = new RankedSemanticProfile(ontology);
		instance.add(document1);
		instance2.add(document2);
		instance2.add(document3);
		instance2.add(document4);
		instance.merge(instance2);
		profile = this.instance.getInnerProfile(true);

		for (Object concept : profile.keys()) {
			assertEquals(expected.get(concept), profile.get(concept), 0.0000001);
		}
	}
}
