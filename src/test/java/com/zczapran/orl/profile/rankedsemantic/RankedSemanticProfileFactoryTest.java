package com.zczapran.orl.profile.rankedsemantic;

import com.zczapran.orl.ontology.Ontology;
import com.zczapran.orl.ontology.SimpleOntology;
import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class RankedSemanticProfileFactoryTest {

	private RankedSemanticProfileFactory object;

	public RankedSemanticProfileFactoryTest() {
	}

	@Before
	public void setUp() {
		Ontology ontology = new SimpleOntology();
		object = new RankedSemanticProfileFactory(ontology);
	}

	/**
	 * Test of getProfile method, of class RankedSemanticProfileFactory.
	 */
	@Test
	public void testGetProfile() {
		RankedSemanticProfile profile1 = object.getProfile();
		RankedSemanticProfile profile2 = object.getProfile();
		assertNotSame(profile1, profile2);
	}

}
