package com.zczapran.orl.profile.vectorbased.weightingscheme;

import com.zczapran.orl.profile.vectorbased.corpus.Corpus;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
@RunWith(JUnit4.class)
public class NoDocumentFrequencyTest {

	/**
	 * Test of getWeight method, of class NoDocumentFrequency.
	 */
	@Test
	public void testGetWeight() {
		DocumentFrequencyScheme object = new NoDocumentFrequency();
		Corpus corpus = mock(Corpus.class);
		when(corpus.getDocumentCount(0)).thenReturn(1);
		when(corpus.getDocumentCount(1)).thenReturn(2);
		when(corpus.getDocumentCount(2)).thenReturn(0);
		assertEquals(1.0, object.getWeight(corpus, 0), 0.0001);
		assertEquals(1.0, object.getWeight(corpus, 1), 0.0001);
		assertEquals(1.0, object.getWeight(corpus, 2), 0.0001);
	}

}