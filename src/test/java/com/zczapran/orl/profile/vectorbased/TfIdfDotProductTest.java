package com.zczapran.orl.profile.vectorbased;

import com.zczapran.orl.profile.vectorbased.corpus.Corpus;
import java.util.HashMap;
import static org.junit.Assert.*;
import org.junit.Test;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class TfIdfDotProductTest {

	/**
	 * Test of calculate method, of class TfIdfDotProduct.
	 */
	@Test
	public void testCalculate() {
		Corpus corpus = mock(Corpus.class);
		when(corpus.getInvertedDocumentFrequency(anyInt())).thenReturn(1.0);
		TfIdfDotProduct object = new TfIdfDotProduct(corpus);

		TermVector a = mock(TermVector.class);
		HashMap<Integer, Integer> aVector = new HashMap<>();
		aVector.put(1, 2);
		aVector.put(4, 1);
		when(a.iterator()).thenReturn(aVector.entrySet().iterator());
		when(a.getTermFrequency(anyInt())).thenReturn(0.0);
		when(a.getTermFrequency(1)).thenReturn(0.5);
		when(a.getTermFrequency(4)).thenReturn(0.3);

		TermVector b = mock(TermVector.class);
		HashMap<Integer, Integer> bVector = new HashMap<>();
		bVector.put(1, 2);
		bVector.put(3, 1);
		when(b.getTermFrequency(anyInt())).thenReturn(0.0);
		when(b.getTermFrequency(1)).thenReturn(0.7);
		when(b.getTermFrequency(3)).thenReturn(0.2);
		when(b.iterator()).thenReturn(bVector.entrySet().iterator());

		assertEquals(0.35, object.calculate(a, b), 0.0001);
	}

}
