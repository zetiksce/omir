package com.zczapran.orl.profile.vectorbased.weightingscheme;

import com.zczapran.orl.profile.vectorbased.TermVector;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class LogarithmTermFrequencyTest {

	/**
	 * Test of getWeight method, of class LogarithmTermFrequency.
	 */
	@Test
	public void testGetWeight() {
		TermFrequencyScheme object = new LogarithmTermFrequency();
		TermVector vector = mock(TermVector.class);
		when(vector.getTermCount(0)).thenReturn(1);
		when(vector.getTermCount(1)).thenReturn(25);
		when(vector.getTermCount(2)).thenReturn(0);
		assertEquals(1.0, object.getWeight(vector, 0), 0.0001);
		assertEquals(4.2188758, object.getWeight(vector, 1), 0.0001);
		assertEquals(0.0, object.getWeight(vector, 2), 0.0001);
	}

}