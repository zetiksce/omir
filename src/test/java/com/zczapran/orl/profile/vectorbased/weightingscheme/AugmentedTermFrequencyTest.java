package com.zczapran.orl.profile.vectorbased.weightingscheme;

import com.zczapran.orl.profile.vectorbased.TermVector;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class AugmentedTermFrequencyTest {

	/**
	 * Test of getWeight method, of class AugmentedTermFrequency.
	 */
	@Test
	public void testGetWeight() {
		TermFrequencyScheme object = new AugmentedTermFrequency();
		TermVector vector = mock(TermVector.class);
		when(vector.getTermCount(0)).thenReturn(1);
		when(vector.getTermCount(1)).thenReturn(2);
		when(vector.getTermCount(2)).thenReturn(0);
		when(vector.getMaxTermCount()).thenReturn(2);
		assertEquals(0.75, object.getWeight(vector, 0), 0.0001);
		assertEquals(1.0, object.getWeight(vector, 1), 0.0001);
		assertEquals(0.5, object.getWeight(vector, 2), 0.0001);
	}

}