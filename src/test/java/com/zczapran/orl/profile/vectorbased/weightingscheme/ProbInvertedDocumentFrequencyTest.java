package com.zczapran.orl.profile.vectorbased.weightingscheme;

import com.zczapran.orl.profile.vectorbased.corpus.Corpus;
import static org.junit.Assert.assertEquals;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
@RunWith(JUnit4.class)
public class ProbInvertedDocumentFrequencyTest {

	@Rule
	public ExpectedException exception = ExpectedException.none();
	/**
	 * Test of getWeight method, of class ProvInvertedDocumentFrequency.
	 */
	@Test
	public void testGetWeight() {
		DocumentFrequencyScheme object = new ProbInvertedDocumentFrequency();
		Corpus corpus = mock(Corpus.class);
		when(corpus.getDocumentCount(0)).thenReturn(1);
		when(corpus.getDocumentCount(1)).thenReturn(2);
		when(corpus.getDocumentCount(2)).thenReturn(0);
		when(corpus.size()).thenReturn(6);
		assertEquals(1.6094379124341003, object.getWeight(corpus, 0), 0.0001);
		assertEquals(0.6931471805599453, object.getWeight(corpus, 1), 0.0001);
		exception.expect(RuntimeException.class);
		object.getWeight(corpus, 2);
	}

}