package com.zczapran.orl.profile.weighted;

import com.zczapran.orl.document.DocumentMock;
import com.zczapran.orl.profile.ProfileMock;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class WeightedDecoratorTest {

	private WeightedDecorator<DocumentMock, ProfileMock> object;
	private ProfileMock profile;

	@Before
	public void setUp() {
		profile = new ProfileMock();
		object = new WeightedDecorator<>(profile, 10.0, 2.0);
	}

	@After
	public void tearDown() {
	}

	/**
	 * Test of weight method, of class WeightedDecorator.
	 */
	@Test
	public void testWeight() {
		assertEquals(10.0, object.weight(), 0.01);
		object.increaseWeight(0.5);
		assertEquals(10.5, object.weight(), 0.01);
	}

	/**
	 * Test of add method, of class WeightedDecorator.
	 */
	@Test
	public void testAdd() {
		assertEquals(10.0, object.weight(), 0.01);
		DocumentMock document = new DocumentMock("uri1");
		object.add(document);
		assertEquals(12.0, object.weight(), 0.01);
	}

	/**
	 * Test of score method, of class WeightedDecorator.
	 */
	@Test
	public void testScore() {
		DocumentMock document = new DocumentMock("uri1");
		double score = 0.5;
		profile.setScore(document, score);
		assertEquals(0.5, object.score(document), 0.01);
		assertEquals(profile.score(document), object.score(document), 0.01);
	}

	/**
	 * Test of size method, of class WeightedDecorator.
	 */
	@Test
	public void testSize() {
		assertEquals(0, object.size());
		DocumentMock document = new DocumentMock("uri1");
		object.add(document);
		assertEquals(1, object.size());
	}

	/**
	 * Test of increaseWeight method, of class WeightedDecorator.
	 */
	@Test
	public void testIncreaseWeight() {
		double currentWeight = object.weight();
		object.increaseWeight(2.5);
		assertEquals(currentWeight + 2.5, object.weight(), 0.01);
	}

}