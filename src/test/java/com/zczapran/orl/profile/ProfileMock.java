package com.zczapran.orl.profile;

import com.zczapran.orl.document.DocumentMock;
import java.util.*;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class ProfileMock implements Profile<DocumentMock> {

	protected Map<DocumentMock,Double> scoredDocuments = new HashMap<>();
	protected Set<DocumentMock> documents = new HashSet<>();
	final private Random rand = new Random();

	@Override
	public void add(DocumentMock document) {
		documents.add(document);
	}

	@Override
	public double score(DocumentMock document) {
		if (documents.contains(document)) {
			return 1.0;
		} else if (scoredDocuments.containsKey(document)) {
			return scoredDocuments.get(document);
		} else {
			double score = rand.nextDouble();
			scoredDocuments.put(document, score);
			return score;
		}
	}

	public void setScore(DocumentMock document, double score) {
		scoredDocuments.put(document, score);
	}

	@Override
	public int size() {
		return documents.size();
	}

}