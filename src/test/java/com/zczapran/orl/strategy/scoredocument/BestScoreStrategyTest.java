package com.zczapran.orl.strategy.scoredocument;

import com.zczapran.orl.document.Document;
import com.zczapran.orl.profile.Profile;
import com.zczapran.orl.strategy.adddocument.helper.Scorer;
import java.util.LinkedList;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class BestScoreStrategyTest {

	/**
	 * Test of score method, of class BestScoreStrategy.
	 */
	@Test
	public void testScore() {
		Scorer<Document, Profile<Document>> scorer = mock(Scorer.class);
		ScoreDocumentStrategy<Document, Profile<Document>> strategy = new BestScoreStrategy<>(scorer);
		Document document = mock(Document.class);
		List<Profile<Document>> profiles = new LinkedList<>();
		double[] scores = {3.0, 5.0};
		when(scorer.score(profiles, document)).thenReturn(scores);
		assertEquals(5.0, strategy.score(profiles, document), 0.0001);
	}

}
