package com.zczapran.orl.strategy.scoredocument;

import com.zczapran.orl.document.DocumentMock;
import com.zczapran.orl.profile.ProfileMock;
import java.util.LinkedList;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * Test for AverageWeightScoreStrategy.
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class AverageWeightScoreStrategyTest {

	/**
	 * Test of score method, of class AverageWeightScoreStrategy.
	 */
	@Test
	public void testScore() {
		AverageWeightScoreStrategy<DocumentMock, ProfileMock> object = new AverageWeightScoreStrategy<>();
		List<ProfileMock> profiles = new LinkedList<>();
		DocumentMock document = new DocumentMock("uri1");

		ProfileMock profile1 = new ProfileMock();
		profile1.add(new DocumentMock("uri2"));
		profile1.add(new DocumentMock("uri3"));
		profile1.setScore(document, 0.8);
		profiles.add(profile1);

		ProfileMock profile2 = new ProfileMock();
		profile2.add(new DocumentMock("uri4"));
		profile2.setScore(document, 0.4);
		profiles.add(profile2);

		ProfileMock profile3 = new ProfileMock();
		profile3.add(new DocumentMock("uri5"));
		profile3.setScore(document, 0.2);
		profiles.add(profile3);

		assertEquals((0.8 * 2 + 0.4 * 1 + 0.2 * 1) / 4, object.score(profiles, document), 0.01);
	}

}
