package com.zczapran.orl.strategy.adddocument.helper;

import com.zczapran.orl.profile.Scorable;
import com.zczapran.orl.strategy.adddocument.helper.LinearScoreModifier;
import com.zczapran.orl.strategy.adddocument.helper.ScoreModifier;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class LinearScoreModifierTest {

	/**
	 * Test of modify method, of class LinearScoreModifier.
	 */
	@Test
	public void testModify() {
		ScoreModifier<Scorable> scoreModifier = new LinearScoreModifier<>();
		Scorable profile1 = mock(Scorable.class);
		Scorable profile2 = mock(Scorable.class);
		when(profile1.score(profile2)).thenReturn(0.5);

		assertEquals(0.5, scoreModifier.modify(profile1, profile2), 0.0001);
	}

}
