package com.zczapran.orl.strategy.adddocument.helper;

import com.zczapran.orl.document.ConceptDocument;
import com.zczapran.orl.document.SimpleConceptDocument;
import com.zczapran.orl.ontology.Ontology;
import com.zczapran.orl.ontology.SimpleOntology;
import com.zczapran.orl.profile.rankedsemantic.RankedSemanticProfile;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertArrayEquals;
import org.junit.Test;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class ParallelScorerTest {

	/**
	 * Test of score method, of class ClosestProfile.
	 */
	@Test
	public void testScore() {
		Ontology ontology = new SimpleOntology();
		List<RankedSemanticProfile> profiles = new ArrayList<>();
		profiles.add(new RankedSemanticProfile(ontology));
		profiles.add(new RankedSemanticProfile(ontology));
		profiles.add(new RankedSemanticProfile(ontology));
		profiles.add(new RankedSemanticProfile(ontology));
		profiles.add(new RankedSemanticProfile(ontology));
		ConceptDocument document1 = new SimpleConceptDocument("uri1", new String[]{"yahoo", "obama", "china"});
		ConceptDocument document2 = new SimpleConceptDocument("uri2", new String[]{"google", "apple", "toyota"});
		ConceptDocument document3 = new SimpleConceptDocument("uri3", new String[]{"a", "b", "c"});
		ConceptDocument document4 = new SimpleConceptDocument("uri4", new String[]{"d", "e", "f"});
		ConceptDocument document5 = new SimpleConceptDocument("uri5", new String[]{"d", "e", "f"});
		profiles.get(0).add(document1);
		profiles.get(1).add(document2);
		profiles.get(2).add(document3);
		profiles.get(3).add(document4);
		profiles.get(4).add(document5);

		ParallelScorer<ConceptDocument, RankedSemanticProfile> instance = new ParallelScorer<>();
		assertArrayEquals(new double[]{0.33, 0.66, 0.00, 0.00, 0.00}, instance.score(profiles, new SimpleConceptDocument("uri4", new String[]{"google", "apple", "yahoo"})), 0.01);
		assertArrayEquals(new double[]{0.33, 0.00, 0.66, 0.00, 0.00}, instance.score(profiles, new SimpleConceptDocument("uri5", new String[]{"a", "b", "yahoo"})), 0.01);
	}

}
