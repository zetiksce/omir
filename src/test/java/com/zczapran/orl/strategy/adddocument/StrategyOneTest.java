package com.zczapran.orl.strategy.adddocument;

import com.zczapran.orl.document.ConceptDocument;
import com.zczapran.orl.document.SimpleConceptDocument;
import com.zczapran.orl.ontology.OntologyMock;
import com.zczapran.orl.profile.ProfileFactory;
import com.zczapran.orl.profile.rankedsemantic.RankedSemanticProfile;
import com.zczapran.orl.profile.rankedsemantic.RankedSemanticProfileFactory;
import com.zczapran.orl.strategy.adddocument.helper.LogarithmicScoreModifier;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
public class StrategyOneTest {

	private StrategyOne<ConceptDocument, RankedSemanticProfile> strategy;
	private ProfileFactory<RankedSemanticProfile> factory;

	@Before
	public void setUp() {
		factory = new RankedSemanticProfileFactory(new OntologyMock());
	}

	/**
	 * Test of add method, of class StrategyOne.
	 */
	@Test
	public void testAdd() {
		strategy = new StrategyOne(factory, 3, new LogarithmicScoreModifier());

		ConceptDocument document1 = new SimpleConceptDocument("uri1", new String[]{"yahoo", "toyota"});
		ConceptDocument document2 = new SimpleConceptDocument("uri2", new String[]{"yahoo", "obama", "china"});
		ConceptDocument document3 = new SimpleConceptDocument("uri3", new String[]{"yahoo", "obama"});
		ConceptDocument document4 = new SimpleConceptDocument("uri4", new String[]{"yahoo"});

		List<RankedSemanticProfile> profiles = new ArrayList<>();
		strategy.add(profiles, document4);
		assertEquals(1, profiles.size());
		strategy.add(profiles, document3);
		assertEquals(2, profiles.size());
		strategy.add(profiles, document2);
		assertEquals(3, profiles.size());
		strategy.add(profiles, document1);
		assertEquals(3, profiles.size());
	}

}