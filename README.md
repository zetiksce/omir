On-line Recommender Library
==================================

Easy to use library that provides fully configurable recommending algorithms.

Example usage:
--------------

    // you can define related concepts in the ontology object
    Ontology ontology = new SimpleOntology();
    
    Profile<ConceptDocument> profile = new RankedSemanticProfile(ontology);
    
    // create some documents
    ConceptDocument document1 = new SimpleConceptDocument("uri1", new String[]{"git", "github", "java"});
    ConceptDocument document2 = new SimpleConceptDocument("uri2", new String[]{"git", "github", "maven"});
    ConceptDocument document3 = new SimpleConceptDocument("uri3", new String[]{"git", "java"});
    ConceptDocument document4 = new SimpleConceptDocument("uri4", new String[]{"git"});
    
    // add documents to the profile
    profile.add(document1);
    profile.add(document2);
    profile.add(document3);
    profile.add(document4);
    
    // create document to score
    ConceptDocument document5 = new SimpleConceptDocument("uri5", new String[]{"git", "github"});
    
    // calculate score for the document and profile
    double score = profile.score(document5);